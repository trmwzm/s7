SRC=s7.c
OBJ=$(SRC:.c=.o)
CC?=cc

EZ_PATH=/usr/local

CFLAGS+=-I$(EZ_PATH)/include

s7i: $(OBJ) s7i.o
	$(CC) -O3 -o s7i $(OBJ) s7i.o $(EZ_PATH)/lib/libez.a -lm

static: $(OBJ)
	ar rcs libs7.a $(OBJ) $(EZ_PATH)/lib/libez.a

%.o : %.c
	$(CC) -c -fpic -O3 $*.c $(CFLAGS) $(LDFLAGS)
