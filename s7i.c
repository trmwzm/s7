#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <unistd.h>
#include <fcntl.h>

#include "s7.h"

#include <ez/sds.h>
#include <ez/linenoise.h>

int main(int argc, char **argv){
    s7_scheme *ctx = s7_init();

    bool read_stdin = false;
    char *infile = NULL;

    int c;
    while((c = getopt(argc, argv, "if:")) != -1){
        switch(c){
        case 'i':
            read_stdin = true;
            break;
        case 'f':
            infile = optarg;
            break;
        }
    }

    if (read_stdin || infile != NULL){
        char buf[1024];
        long long len = 0;

        int fd = STDIN_FILENO;
        sds inp = sdsnew(NULL);

        if (infile){
            fd = open(infile, O_RDONLY);
            if (fd < 0){
                fputs("ERROR: Couldn't open input file", stderr);
                goto End;
            }
        }

        while((len = read(fd, buf, 1024)) > 0){
            inp = sdscatlen(inp, buf, len);
        }

        s7_eval_c_string(ctx, inp);

        if (infile){
            close(fd);
        }

        goto End;
    }

    while(true){
        char *line = linenoise("s7> ");
        if (!line){
            continue;
        }

        if (strncmp(line, "exit", 5) == 0){
            free(line);
            break;
        }

        s7_pointer res = s7_eval_c_string(ctx, line);

        if (res){
            s7_display(ctx, res, s7_current_output_port(ctx));
            s7_flush_output_port(ctx, s7_current_output_port(ctx));
            putchar('\n');
        }

        linenoiseHistoryAdd(line);

        free(line);
    }

End:
    free(ctx);
    return 0;
}
